<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\Type\User\UserType;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{

    public function create(EntityManagerInterface $em):Response
    {
        $user = new User();
        $user
            ->setName('john');

        $em->persist($user);
        $em->flush();
        return new Response();
    }


    /**
     * @Route("/uscr", name="us_cr", methods={"GET"})
     */
    public function createUser(Request $request, UserRepository $repository):Response
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            dd($user);
        }

        return $this->render(view: 'create.html.twig', parameters: ['form'=> $form->createView()]);

    }

    /**
     * @Route("/users/{id}/tours", name="user_autos", methods={"GET"})
     */
    public function getUserTours(int $id, EntityManagerInterface $em): Response
    {
        $user = $em->getRepository(User::class)->find($id);

        foreach ($user->GetTours() as $tour){
            dd($tour);
        }

    }

}