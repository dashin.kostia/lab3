<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Tour;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TourController extends AbstractController
{
    /**
     * @Route("/tours", name="add_tour", methods={"GET"})
     */

    public function addTour(EntityManagerInterface $em): Response
    {
        $tour = new Tour();
        $tour->setName('Albania');

        $em->persist($tour);
        $em->flush();

        return new Response((string) $tour->getId());

    }


    /**
     * @Route("/tours/users", name="tour_user", methods={"GET"})
     */

    public function addUserToTour(EntityManagerInterface $em): Response
    {
        $tour = $em->getRepository(Tour::class)->find(1);
        $user = $em->getRepository(User::class)->find(1);

        $user ->addTour($tour);

        $em->persist($user);
        $em->flush();

        return new Response();

    }
}